from flask import Flask, render_template, request
app = Flask(__name__)
import grequests
from gevent import monkey
from flask import jsonify
import json

monkey.patch_all()
cache = {}

@app.route('/')
def hello_world():
    return render_template('app.html')

@app.route('/compare_prices', methods=['POST'])
def compare_hotels():
    city, checkin, checkout = request.form["city"], request.form["checkin"], request.form["checkout"]

    if (city, checkin, checkout) in cache:
        print("retrieve from cache")
        return  render_template('demo.html', name='world', data=cache[(city, checkin, checkout)])
    # change url to https://experimentation.snaptravel.com/interview/hotels
    urls = ['http://127.0.0.1:5000/test', 'http://127.0.0.1:5000/test']

    # change credentials for https://experimentation.snaptravel.com/interview/hotels
    username = ""
    password = ""

    params = [{'city': city, 'checkin':checkin, 'checkout': checkout, 'provider': 'snaptravel', 'username': username, 'password': password},
    {'city':city, 'checkin': checkin, 'checkout': checkout, 'provider': 'retail', 'username': username, 'password': password}]

    #grequest requests asynchronously i believe
    rs = (grequests.post(urls[i], data=params[i]) for i in range(len(urls)))
    responses = grequests.map(rs)

    hotels_snap = json.loads(responses[0].content)
    hotels_retail = json.loads(responses[1].content)

    snap_price = {}
    common_hotels = []
    for i in range(len(hotels_snap)):
        snap_price[hotels_snap[i]["id"]] = hotels_snap[i]["price"]

    for i in range(len(hotels_retail)):
        hotel_id = hotels_retail[i]["id"]
        hotel = hotels_retail[i]
        if hotel_id in snap_price:
            price_retail = hotel.pop("price")
            hotel["price_snap"] = snap_price[hotel_id]
            hotel["price_retail"] = price_retail
            common_hotels.append(hotel)

    cache[(city, checkin, checkout)] = common_hotels

    return render_template('demo.html', name='world', data=common_hotels)

@app.route('/test', methods=['POST'])
def test():
    isSnaptravel = request.form['provider'] == "snaptravel"
    price = 112.33 if isSnaptravel else 132.11

    hotel1 = {"id": 12, "hotel_name": "Center Hilton", "num_reviews": 209, "price": price, "address": '12 Wall Street, Very Large City', "num_stars": 4, "amenities" : ['Wi-Fi', 'Parking'], "image_url" : 'https://images.trvl-media.com/hotels/1000000/20000/19600/19558/19558_410_b.jpg'}
    hotel2 = {"id": 13, "hotel_name": "North Hilton", "num_reviews": 511, "price": price, "address": '99 Bayview Ave, Cool Town', "num_stars": 4, "amenities" : ['Wi-Fi', 'Parking'], "image_url" : 'https://images.trvl-media.com/hotels/1000000/20000/19600/19558/19558_410_b.jpg'}
    hotel3 = {"id": 14, "hotel_name": "Hotel Denouement", "num_reviews": 1, "price": price, "address": 'Somewhere', "num_stars": 1, "amenities" : [], "image_url" : 'https://vignette.wikia.nocookie.net/snicket/images/4/41/Dentel.jpeg/revision/latest?cb=20190215063706'}

    if request.form['provider'] == "snaptravel":
        return jsonify([hotel1, hotel2])
    return jsonify([hotel1, hotel2, hotel3])
